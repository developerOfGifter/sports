# Sports
Short Swift Tech Challenge  (please don’t spend over one hour on it – we’re keeping that in mind when evaluating the code.)
 For this exercise consulting the internet is not cheating. You are also free to use whatever third-party libraries you may find useful.

Take the below sports.json file and do the following:
1. Create 3 classes
1. Sport
2. Game
3. Team
2. Create a “SportsService” that will handle parsing the JSON to objects
3. Translate the JSON into the objects created above *hint: checkout Codable*
4. Create a UIViewController and populate the sports into a table. *hint: SportsService!*
5. Give the user the ability to add a new sport. Specifically, allow them to enter the sport name and populate fake games based solely on the number of games entered.
Feel free to add your own style / twist to this task!
Bonus: Create a computed property on Game that takes the gameTimeUtc and transforms it to a readable date!

When you’re finished please return the entire project to your HR contact either via github/bitbucket link, a hosted file repository (dropbox, box), or a zipped file directly through email if not too large.

Good luck!
