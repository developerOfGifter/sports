//
//  AddGameViewController.swift
//  Sports
//
//  Created by Varun Goyal on 6/7/19.
//  Copyright © 2019 png. All rights reserved.
//

import UIKit

protocol AddGameViewControllerDelegate: class {
    func didAdd(sport: Sport)
}

class AddGameViewController: UIViewController {
    public var delegate: AddGameViewControllerDelegate?
    private var games = [Game]()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sportName: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: "done", style: .done, target: self, action: #selector(doneButtonPressed))
    }
    
    @objc func doneButtonPressed() {
        guard let sportName = sportName.text else { return }
        let randomId = Int.random(in: 0...600)
        delegate?.didAdd(sport: Sport(id: randomId, name: sportName, games: games))
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addGamePressed(_ sender: UIButton) {
        games.append(Game())
        tableView.reloadData()
    }
}


extension AddGameViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return games.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? AddGameCell else {
            fatalError("AddGameViewController: could not dequeue cell")
        }
        cell.game = games[indexPath.row]
        cell.presentingViewController = self
        cell.indexPath = indexPath
        cell.delegate = self
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            games.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
}

extension AddGameViewController: AddGameCellDelegate {
    func didChange(teamB: String, indexPath: IndexPath) {
        games[indexPath.row].teams.removeAll(where: { $0.id == 1})
        let team = Team.init(id: 1, name: teamB)
        games[indexPath.row].teams.append(team)
    }
    
    func didChange(teamA: String, indexPath: IndexPath) {
        games[indexPath.row].teams.removeAll(where: { $0.id == 0})
        let team = Team.init(id: 0, name: teamA)
        games[indexPath.row].teams.append(team)
    }
    
    func didChange(date: Date, indexPath: IndexPath) {
        games[indexPath.row].gameTimeUtc = String(date.timeIntervalSince1970)
    }
}
