//
//  GameViewController.swift
//  Sports
//
//  Created by Varun Goyal on 6/7/19.
//  Copyright © 2019 png. All rights reserved.
//

import UIKit

class GameViewController: UIViewController {
    public var games: [Game]!   // dependency injection.
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if games.isEmpty {
            fatalError("GameViewController: there are no games to display.")
        }
    }
}


extension GameViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return games.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") else {
            fatalError("cell cannot be dequeued.")
        }
        
        if let teamA = games[indexPath.row].teams.first?.name,
            let teamB = games[indexPath.row].teams.last?.name {
            cell.textLabel?.text = "\(teamA) Vs \(teamB)"
            cell.detailTextLabel?.text = games[indexPath.row].readableGameTime
        }
        
        return cell
    }
}
