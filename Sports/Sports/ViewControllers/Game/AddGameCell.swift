//
//  AddGameCell.swift
//  Sports
//
//  Created by Varun Goyal on 6/9/19.
//  Copyright © 2019 png. All rights reserved.
//

import UIKit

class AddGameCell: UITableViewCell {
    public var game: Game!
    public var presentingViewController: UIViewController!
    public var indexPath: IndexPath!
    public var delegate: AddGameCellDelegate?
    
    @IBOutlet weak var teamATextField: UITextField!
    @IBOutlet weak var teamBTextField: UITextField!
    @IBOutlet weak var dateButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        teamATextField.delegate = self
        teamBTextField.delegate = self
    }
    
    @IBAction func enterTheDateTapped(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "GameStoryboard", bundle: nil)
        guard let datePickerViewController = storyboard.instantiateViewController(withIdentifier: "datePickerViewController") as? DatePickerViewController else { return }
        datePickerViewController.delegate = self
        presentingViewController.present(datePickerViewController, animated: true, completion: nil)
    }
}

extension AddGameCell: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let updatedText = textField.text else { return }
        
        if textField == teamATextField {
            delegate?.didChange(teamA: updatedText, indexPath: indexPath)
        } else if textField == teamBTextField {
            delegate?.didChange(teamB: updatedText, indexPath: indexPath)
        }
    }
}

extension AddGameCell: DatePickerViewControllerDelegate {
    func didSelect(date: Date) {
        dateButton.setTitle(format(date: date), for: UIControl.State.normal)
        delegate?.didChange(date: date, indexPath: indexPath)
        
    }
}

protocol AddGameCellDelegate {
    func didChange(date: Date, indexPath: IndexPath)
    func didChange(teamA: String, indexPath: IndexPath)
    func didChange(teamB: String, indexPath: IndexPath)
}

