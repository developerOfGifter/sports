//
//  DatePickerViewController.swift
//  Sports
//
//  Created by Varun Goyal on 6/9/19.
//  Copyright © 2019 png. All rights reserved.
//

import UIKit

class DatePickerViewController: UIViewController {
    public var delegate: DatePickerViewControllerDelegate?
    @IBOutlet weak var datePicker: UIDatePicker!
    
    @IBAction func doneButtonPressed(_ sender: Any) {
        delegate?.didSelect(date: datePicker.date)
        self.dismiss(animated: true, completion: nil)
    }
}


protocol DatePickerViewControllerDelegate {
    func didSelect(date: Date)
}
