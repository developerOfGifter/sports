//
//  LandingViewController.swift
//  Sports
//
//  Created by Varun Goyal on 6/7/19.
//  Copyright © 2019 png. All rights reserved.
//

import UIKit

class LandingViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    private var sports: [Sport]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let allSports = SportsService().getData() {
            sports = allSports.sports
            tableView.reloadData()
        }
        title = "Sports"
    }
    
    @IBAction func addNewGame(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: "GameStoryboard", bundle: nil)
        guard let addGameViewController = storyboard.instantiateViewController(withIdentifier: "addGameViewController") as? AddGameViewController else { return }
        addGameViewController.delegate = self
        self.navigationController?.pushViewController(addGameViewController, animated: true)
    }
    
}

extension LandingViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let games = sports?[indexPath.row].games, !games.isEmpty else { return }
        
        let storyboard = UIStoryboard(name: "GameStoryboard", bundle: nil)
        guard let gameViewController = storyboard.instantiateViewController(withIdentifier: "gameViewController") as? GameViewController else { return }
        gameViewController.games = sports?[indexPath.row].games
        gameViewController.title = sports?[indexPath.row].name
        self.navigationController?.pushViewController(gameViewController, animated: true)
    }
}

extension LandingViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sports?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") else {
            print("could not dequeue cell")
            return UITableViewCell()
        }
        
        cell.textLabel?.text = sports?[indexPath.row].name
        
        if let games = sports?[indexPath.row].games, !games.isEmpty {
            cell.accessoryType = .disclosureIndicator
        } else {
            cell.accessoryType = .none
        }
        
        return cell
    }
}

extension LandingViewController: AddGameViewControllerDelegate {
    func didAdd(sport: Sport) {
        sports?.append(sport)
        tableView.reloadData()
    }
    
    
}
