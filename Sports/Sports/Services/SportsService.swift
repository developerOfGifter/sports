//
//  SportsService.swift
//  Sports
//
//  Created by Varun Goyal on 6/7/19.
//  Copyright © 2019 png. All rights reserved.
//

import Foundation

private struct mockFile {
    static let name = "sports"
    static let type = "json"
}

class SportsService {
    func getData() -> Sports? {
        do {
            if let path = Bundle.main.path(forResource: mockFile.name, ofType: mockFile.type) {
                let data = try Data(contentsOf: URL(fileURLWithPath: path))
                return try JSONDecoder().decode(Sports.self, from: data)
            }
        } catch {
            print(error)
        }
        return nil
    }
}
