//
//  Game.swift
//  Sports
//
//  Created by Varun Goyal on 6/7/19.
//  Copyright © 2019 png. All rights reserved.
//

import Foundation
private let formatter = DateFormatter()

struct Game: Codable {
    var id: Int?
    var teams = [Team]()
    var gameTimeUtc: String?
    var readableGameTime: String? {
        if let gameTimeUtc = gameTimeUtc, let timeStampDouble = Double(gameTimeUtc) {
            return format(date: Date(timeIntervalSince1970: timeStampDouble))
        }
        return nil
    }
}


public func format(date: Date) -> String {
    formatter.dateFormat = "MMMM-dd'th at' hh:mm a"
    return formatter.string(from: date)
}
