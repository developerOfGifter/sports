//
//  Team.swift
//  Sports
//
//  Created by Varun Goyal on 6/7/19.
//  Copyright © 2019 png. All rights reserved.
//

import Foundation

struct Team: Codable {
    var id: Int?
    var name: String?
}
