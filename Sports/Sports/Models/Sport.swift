//
//  Sport.swift
//  Sport
//
//  Created by Varun Goyal on 6/7/19.
//  Copyright © 2019 png. All rights reserved.
//

import Foundation

struct Sports: Codable {
    var sports: [Sport]?
}

struct Sport: Codable {
    var id: Int?
    var name: String?
    var games: [Game]?
}
